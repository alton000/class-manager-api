import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class BasicEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;
}