import { Entity, Column, ManyToOne } from "typeorm";
import { BasicEntity } from "../basicEntity.entity";

@Entity()
export class Class extends BasicEntity{
    @Column()
    course: string;
}