import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CourseService } from './course/course.service';
import { TestService } from './test/test.service';
import { ClassService } from './class/class.service';
import { TeacherService } from './teacher/teacher.service';
import { StudentService } from './student/student.service';
import { CourseController } from './course/course.controller';
import { StudentController } from './student/student.controller';
import { TeacherController } from './teacher/teacher.controller';
import { ClassModule } from './class/class.module';
import { CourseModule } from './course/course.module';
import { StudentModule } from './student/student.module';
import { TeacherModule } from './teacher/teacher.module';
import { TestModule } from './test/test.module';

@Module({
  imports: [ClassModule, CourseModule, StudentModule, TeacherModule, TestModule],
  controllers: [AppController, CourseController, StudentController, TeacherController],
  providers: [AppService, CourseService, TestService, ClassService, TeacherService, StudentService],
})
export class AppModule {}
