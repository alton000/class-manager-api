import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { BasicEntity } from "../basicEntity.entity";

@Entity()
export class Teacher extends BasicEntity{
    
    @Column()
    courses: string;

}