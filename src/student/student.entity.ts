import { Entity, Column, ManyToMany, OneToMany } from "typeorm";
import { BasicEntity } from "../basicEntity.entity";

@Entity()
export class Student extends BasicEntity{
    @Column()
    courses: string;

    @Column()
    test_student: string;
}